Python
======

Install Package from GIT
------------------------

::

    sudo pip install git+https://github.com/candidtim/vagrant-appindicator.git

Install Python3 Package
-----------------------

`StackOverflow <http://stackoverflow.com/questions/10763440/how-to-install-python3-version-of-package-via-pip-on-ubuntu/12262143#12262143>`__

::

    sudo apt-get install python3-setuptools
    sudo easy_install3 pip
    sudo pip3.4 install <package>
