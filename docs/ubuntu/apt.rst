APT
---

Compare Version Number
~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

    dpkg --compare-versions 1.2.7-0trusty lt 1.2.7+0trusty && echo true
    true
    
    lt: less than
    gt: greater than

Package Information
~~~~~~~~~~~~~~~~~~~

.. code:: bash

    apt-cache policy PACKAGE_NAME

This will give you information about available versions and which
version will be installed and from where:

.. code:: bash

    $ apt-cache policy libqt5opengl5-dev

    libqt5opengl5-dev:
      Installed: 5.2.1+dfsg-1ubuntu14.3
      Candidate: 5.2.1+dfsg-1ubuntu14.3
      Version table:
     *** 5.2.1+dfsg-1ubuntu14.3 0
            500 http://ch.archive.ubuntu.com/ubuntu/ trusty-security/main amd64 Packages
            500 http://ch.archive.ubuntu.com/ubuntu/ trusty-updates/main amd64 Packages
            100 /var/lib/dpkg/status
         5.2.1+dfsg-1ubuntu14 0
            500 http://ch.archive.ubuntu.com/ubuntu/ trusty/main amd64 Packages

Hash Sum Mismatch
~~~~~~~~~~~~~~~~~

.. code:: bash

    sudo rm -rf /var/lib/apt/lists/*
    sudo apt-get clean
    sudo apt-get update

List Packages from Repository
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

List all packages that are installed from a specific repository/ppa.

.. code:: bash

    sudo apt-get install aptitude
    aptitude search "?origin (ethz) ?installed"

List Packages without Repository
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

List all packages that are installed, but do not belong to a
repository/ppa.

.. code:: bash

    sudo apt-get install apt-show-versions
    apt-show-versions | grep 'No available version'
