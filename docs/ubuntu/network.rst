Network
-------

Scan IP Addresses
~~~~~~~~~~~~~~~~~

`Angry IP Scanner`_

.. code:: bash

    sudo nmap -sP 192.168.0.0/24

    sudo arp-scan --retry=8 --ignoredups -I wlan0 192.168.0.0/24

Show DNS
~~~~~~~~

.. code:: bash

    nmcli dev list iface wlan0 | grep IP4.DNS

Set DNS
~~~~~~~

.. code:: bash

    sudo vim /etc/resolv.conf

    nameserver 8.8.8.8
    nameserver 8.8.4.4

    sudo vim /etc/resolvconf/resolv.conf.d/head

Restart Network Interface
~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

    sudo ifdown eth0
    sudo ifup eth0

Terminal Analyse
~~~~~~~~~~~~~~~~

.. code:: bash

    bmon
    tcptrack

Share Internet Over WIFI/Ethernet
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

    iptables -t nat -A POSTROUTING -s 192.168.0.0/24 -o wlan0 -j MASQUERADE
    iptables -t nat -A POSTROUTING -s 10.42.0.0/24 -o eth0 -j MASQUERADE

Server
^^^^^^

.. code:: bash

    auto eth0
    iface eth0 inet static
    address 10.42.0.1
    netmask 255.255.255.0
    network 10.42.0.0

Client
^^^^^^

.. code:: bash

    auto eth0
    iface eth0 inet static
    address 10.42.0.2
    netmask 255.255.255.0
    network 10.42.0.0

IP Tables - Jenkins
~~~~~~~~~~~~~~~~~~~

Add Rules
^^^^^^^^^

`Jenkins tutorial`_

List ``iptables`` rules:

::

     iptables -L -n

Add rules:

::

    sudo iptables -I INPUT 1 -p tcp --dport 8443 -j ACCEPT
    sudo iptables -I INPUT 1 -p tcp --dport 8080 -j ACCEPT
    sudo iptables -I INPUT 1 -p tcp --dport 443 -j ACCEPT
    sudo iptables -I INPUT 1 -p tcp --dport 80 -j ACCEPT

Add forwarding:

::

    sudo iptables -A PREROUTING -t nat -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8080
    sudo iptables -A PREROUTING -t nat -i eth0 -p tcp --dport 443 -j REDIRECT --to-port 8443

Save rules:

::

    sudo sh -c "iptables-save > /etc/iptables.rules"
    sudo apt-get install iptables-persistent

Remove Rules
^^^^^^^^^^^^

`Remove rules`_

.. _Angry IP Scanner: http://angryip.org/download/#linux
.. _Jenkins tutorial: https://wiki.jenkins-ci.org/display/JENKINS/Running+Jenkins+on+Port+80+or+443+using+iptables
.. _Remove rules: https://www.digitalocean.com/community/tutorials/how-to-list-and-delete-iptables-firewall-rules