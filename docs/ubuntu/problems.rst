Problem Solving
---------------

CLion Keyboard ibus Fix
~~~~~~~~~~~~~~~~~~~~~~~

`Source <https://goo.gl/G1vXXy>`_

This solution is if you would like to upgrade ibus in Ubuntu 14.04.4 LTS
to the latest version (at the time of writing is 1.5.11).

I recently had to install Intellij on a clean installed Ubuntu machine.
These are the steps I took:

1. Install the dependencies

   ::

        sudo apt-get install libdconf-dev libnotify-dev intltool libgtk2.0-dev libgtk-3-dev libdbus-1-dev

2. Download the `ibus 1.5.11 source code`_ (linked from `here`_)
3. Extract the files (tar -xvf ibus-1.5.11.tar.gz) and cd into the
   extracted folder
4. While in the ibus source folder, follow the instructions from step 1
   to install ibus 1.5.11:

   ::

       ./configure --prefix=/usr --sysconfdir=/etc && make
       sudo make install

5. Restart Intellij IDE or whichever jetbrain’s IDE that is in question

dbus Fix
~~~~~~~~

Maybe rename ``~/.dbus`` and ``~/.dbus-keyrings``.

.. code:: bash

    sudo service dbus restart
    reboot

Graphics Card Repair (Black Screen)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Remove a (not working) Nvidia graphics card driver:

1. Boot
2. If screen is black, press Ctrl + Alt + F1
3. Login with your username and password

.. code:: bash

    sudo stop lightdm
    sudo apt-get purge nvidia*
    sudo apt-get install xserver-xorg-video-nouveau
    sudo reboot

gvfs Fix
~~~~~~~~

.. code:: bash

    sudo umount ~/.gvfs
    sudo rm -rf ~/.gvfs

usbserial Fix
~~~~~~~~~~~~~

.. code:: bash

    sudo modprobe usbmon
    sudo modprobe usbserial vendor=<VENDOR ID> product=<PRODUCT ID>

.. _ibus 1.5.11 source code: https://github.com/ibus/ibus/releases/download/1.5.11/ibus-1.5.11.tar.gz
.. _here: http://www.linuxfromscratch.org/blfs/view/systemd/general/ibus.html
