Tools
-----

Audio Settings
~~~~~~~~~~~~~~

To change your audio settings in command line, use e.g. alsamixer.

.. code:: bash

    alsamixer

File Manager
~~~~~~~~~~~~

A command line file manager.

.. code:: bash

    sudo apt-get install mc

PDF Editor `PDFtk`_
~~~~~~~~~~~~~~~~~~~

E.g. take some pages and store them in a new PDF.

.. code:: bash

    sudo apt-get install pdftk
    pdftk full.pdf cat 1-5 output outfile_p1-5.pdf

.. _PDFtk: https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/