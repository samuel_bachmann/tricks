Java
----

Change Java Version
~~~~~~~~~~~~~~~~~~~

Download e.g. ``jdk-8u121-linux-x64.tar.gz`` from `Oracle`_ and unzip the folder to e.g. ``/opt/jdk1.8.0_121``.

.. code:: bash

    sudo update-alternatives --install /usr/bin/java java /opt/jdk/jdk1.8.0_121/bin/java 1
    sudo update-alternatives --install /usr/bin/javac javac /opt/jdk/jdk1.8.0_121/bin/javac 1

    sudo update-alternatives --config java
    sudo update-alternatives --config javac

    java -version
    javac -version

.. _Oracle: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html