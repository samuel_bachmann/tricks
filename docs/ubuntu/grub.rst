Grub
----

Remove Old Ubuntu Kernel
~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

    dpkg -l | grep linux-image-

    sudo apt-get purge linux-image-3.5.0-{17,18,19}-generic

Remove Old Ubuntu Kernel
~~~~~~~~~~~~~~~~~~~~~~~~

First remove any leftover temporary files from previous kernel updates.

.. code:: bash

    $ sudo rm -rv ${TMPDIR:-/var/tmp}/mkinitramfs-*

Determine the version number of the currently running kernel, which you DO NOT want to remove.

.. code:: bash

    uname -r

List all the kernels, including the booted one (4.2.0-21-generic in this example), in the package database and their statuses.

.. code:: bash

    dpkg -l | tail -n +6 | grep -E 'linux-image-[0-9]+'

To free space in /boot we'll remove an initrd.img file for a suitable old kernel manually, this is necessary due to a kenel packaging bug.

.. code:: bash

    sudo update-initramfs -d -k 4.2.0-15-generic

Now we'll use dpkg in order to TRY to purge the kernel package for the same old kernel:

.. code:: bash

    sudo dpkg --purge linux-image-4.2.0-15-generic

NOTE: The previous command will probably fail, as there probably is a depending linux-image-extra package installed together with a 'generic' kernel package. In general, the output of the previous command will tell which package you need to remove first. In this example case you would run

.. code:: bash

    sudo dpkg --purge linux-image-4.2.0-15-generic linux-image-extra-4.2.0-15-generic

Finally, we will fix the package installation process that previously failed.

.. code:: bash

    sudo apt-get -f install         ## Try to fix the broken dependency.


Start Custom OS/Kernel
~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

    sudo vim /etc/default/grub

Edit the following line:

.. code:: bash

    GRUB_DEFAULT="Advanced options for Ubuntu>Ubuntu, with Linux 3.13.0-53-generic"

Update grub.

.. code:: bash

    sudo update-grub
