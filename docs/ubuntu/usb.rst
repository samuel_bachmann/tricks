USB
---

USB Information
~~~~~~~~~~~~~~~

Get extended information about an USB device (serial, vendor/product id,
…).

.. code:: bash

    udevadm info -q all -n /dev/ttyUSB0
    // or
    udevadm info --attribute-walk --name=/dev/ttyUSB0

Restart udev.

.. code:: bash

    sudo systemctl restart udev
    sudo udevadm trigger

Get video information.

.. code:: bash

    v4l2-ctl --all -d /dev/video0
    v4l2-ctl --list-formats -d /dev/video0
