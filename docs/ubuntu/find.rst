Find Files
----------

Delete Empty Folders
~~~~~~~~~~~~~~~~~~~~

.. code:: bash

    find . -empty -type d -delete

Delete Temporary Files
~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

    find . -type f -name '*.~' -delete

Large Files
~~~~~~~~~~~

.. code:: bash

    sudo find / -size +1G -ls
