Bluetooth
---------

Connect Logitech MX Master
~~~~~~~~~~~~~~~~~~~~~~~~~~

If you already tried to connect the MX Master, remove the Bluetooth device. And
execute the following steps. `Source <https://goo.gl/hzgMWt>`_

.. code:: bash

    bluetoothctl

    [bluetooth]# power off
    [bluetooth]# power on
    [bluetooth]# scan on
    [bluetooth]# connect XX:XX:XX:XX:XX:XX
    [Arc Touch Mouse SE]# trust
    [Arc Touch Mouse SE]# connect XX:XX:XX:XX:XX:XX
    [Arc Touch Mouse SE]# pair
    [Arc Touch Mouse SE]# unblock
    [Arc Touch Mouse SE]# power off
    [bluetooth]# power on

Connect Headphones
~~~~~~~~~~~~~~~~~~

To ensure the headphones connect with A2DP and not with HSP/HFP.

.. code:: bash

    sudo vim /etc/bluetooth/audio.conf

Add the lines:

.. code:: bash

    [General]
    Disable=Headset

Restart Bluetooth:

.. code:: bash

    sudo service bluetooth restart

