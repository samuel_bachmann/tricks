SSH
---

Start SSH Agent
~~~~~~~~~~~~~~~

.. code:: bash

    eval $(ssh-agent)
    ssh-add
