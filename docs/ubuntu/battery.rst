Battery
-------

`TLP documentation <https://linrunner.de/en/tlp/docs/tlp-configuration.html>`_

Install the following:

.. code:: bash

    sudo add-apt-repository ppa:linrunner/tlp
    sudo apt-get update

    sudo apt-get install tlp tlp-rdw
    sudo apt-get install tp-smapi-dkms acpi-call-dkms

Check which of the above modules (``tp-smapi-dkms acpi-call-dkms``) you need.

.. code:: bash

    sudo tlp-stat -b -s -c

Modify the battery thresholds:

.. code:: bash

    sudo vim /etc/default/tlp

Blacklist the Bluetooth device to avoid disconnections:

.. code:: bash

    $ lsusb

    Bus 003 Device 004: ID 8087:07dc Intel Corp.

add this ``8087:07dc`` to the file ``/etc/default/tlp``

.. code:: bash

    USB_BLACKLIST="8087:07dc"

To (re)start tlp without a reboot:

.. code:: bash

    sudo tlp start

Charge battery full (ignoring the threshold once):

.. code:: bash

    sudo tlp fullcharge

Recalibrate the battery:

.. code:: bash

    sudo tlp recalibrate
