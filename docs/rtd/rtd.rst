ReadTheDocs
===========

Pandoc
------

Use `Pandoc`_ to convert a markdown file to reStructuredText.

.. code:: bash

    pandoc --from markdown --to rst -o common.rst common.md

Or use the `online converter`_.

rst2pdf
-------

Use `rst2pdf`_ to convert a restructuredText document to PDF.

.. code:: bash

    rst2pdf common.rst

Sphinx Documentation
--------------------

ReadTheDocs works best with reStructuredText and `Sphinx`_.

.. _Pandoc: http://pandoc.org/
.. _online converter: http://pandoc.org/try/
.. _rst2pdf: http://rst2pdf.ralsina.me/stories/index.html
.. _Sphinx: http://www.sphinx-doc.org/en/stable/markup/toctree.html