Regex
-----

A collection of regex patterns e.g. for search and replace, tested with JetBrains IDEs.

Replace ``std::cout`` with ``ROS_INFO_STREAM_NAMED()``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Search**

.. code:: bash

    std::cout ([< "^\n.?\[\]&:\(\)'a-zA-Z0-9]{1,});

**Replace**

.. code:: bash

    ROS_INFO_STREAM_NAMED(TAG, TAG $1);
