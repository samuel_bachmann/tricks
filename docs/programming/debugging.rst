Debugging
---------

GDB
~~~

Python
^^^^^^

.. code:: bash

    gdb python
    run /<path to your python script>

Catch and Locate Exceptions
^^^^^^^^^^^^^^^^^^^^^^^^^^^

-  `locate
   exceptions <http://stackoverflow.com/questions/2443135/how-do-i-find-where-an-exception-was-thrown-in-c/26695274#26695274>`__

.. code:: bash

    gdb (python)
    set pagination off
    catch throw
    commands
    backtrace
    continue
    end
    run ...
