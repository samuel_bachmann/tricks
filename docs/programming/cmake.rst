Cmake
-----

Add Headers to IDE
~~~~~~~~~~~~~~~~~~

.. code:: cmake

    file(GLOB_RECURSE HeaderFiles "include/*.h")
    add_custom_target(headers SOURCES ${HeaderFiles})
