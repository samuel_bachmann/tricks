ROS
===

AF\_INET Fix
------------

The AF\_INET error means your ROS instance cannot resolve a hostname. To
solve the AF\_INET problem, export your ROS\_IP address or add your
hostname and IP address to the other computers.

.. code:: bash

    hostname -I
    export ROS_IP=192.168.0.100

CMAKE Arguments
---------------

Append
~~~~~~

.. code:: bash

    catkin config -a --cmake-args -DCMAKE_BUILD_TYPE=Release

Remove
~~~~~~

.. code:: bash

    catkin config -r --cmake-args -DCMAKE_BUILD_TYPE=Release

Deactivate genjava
------------------

.. code:: bash

    echo "export ROS_LANG_DISABLE=genjava" >> ~/catkin_ws/devel/setup.bash

Debug
-----

RQT
~~~

.. code:: bash

    gdb python
    (gdb) run /home/sam/catkin_ws/src/rqt_multiplot/script/rqt_multiplot

Normal
~~~~~~

The ``launch-prefix`` attribute of the ``<node>`` tag that, among other
things, makes it easy to debug a ROS node process. Here are some example
``launch-prefixes`` you might find useful: -
``launch-prefix="xterm -e gdb --args"`` : run your node in a gdb in a
separate xterm window, manually type ``run`` to start it -
``launch-prefix="gdb -ex run --args"`` : run your node in gdb in the
same xterm as your launch without having to type ``run`` to start it -
``launch-prefix="valgrind"`` : run your node in valgrind -
``launch-prefix="xterm -e"`` : run your node in a separate xterm window
- ``launch-prefix="nice"`` : nice your process to lower its CPU usage -
``launch-prefix="screen -d -m gdb --args"`` : useful if the node is
being run on another machine; you can then ssh to that machine and do
``screen -D -R`` to see the gdb session -
``launch-prefix="xterm -e python -m pdb"`` : run your python node a
separate xterm window in pdb for debugging; manually type ``run`` to
start it

`ROS
debug <http://wiki.ros.org/roslaunch/Tutorials/Roslaunch%20Nodes%20in%20Valgrind%20or%20GDB>`__

.. code:: bash

    catkin build --cmake-args -DCMAKE_BUILD_TYPE=Debug

Install Space
-------------

An install space will help you debugging ROS packages in preparation for
a Debian package release.

.. code:: bash

    catkin clean --all
    catkin config --install
    catkin build PACKAGE

Reset Install
~~~~~~~~~~~~~

.. code:: bash

    catkin config --no-install

rosdep
------

.. code:: bash

    rosdep install --from-paths WORKSPACE --ignore-src --rosdistro=ROSDISTRO
    rosdep install rqt_multiplot --ignore-src

ROS MASTER URI
--------------

.. code:: bash

    export ROS_MASTER_URI=http://<your host name or ip address>:11311
