Git
===

Delete Remote Branch
--------------------

.. code:: bash

    git push origin --delete feature/foo

Fix ^M
------

This means you have Windows line endings in your files. To fix call
``dos2unix file.txt``.

.. code:: bash

    sudo apt-get install dos2unix
    dos2unix file.txt

Multiple Users Rights
---------------------

.. code:: bash

    cd /home/john/git
    sudo chown -R :developers "$PWD"
    sudo chmod -R g+swX "$PWD"

Add the following flag to .git/config

.. code:: bash

    [core]
        sharedrepository = 1

New User
--------

.. code:: bash

    git config --set user.name "John Doe"
    git config --set user.email "john.doe@gmail.com"

Soft Reset / Undo Last Commit (if not published yet)
----------------------------------------------------

.. code:: bash

    git reset --soft HEAD~
