Rebuild Launchpad Debian Package (e.g. Backport)
------------------------------------------------

Get Launchpad Sources
~~~~~~~~~~~~~~~~~~~~~

::

    dget https://launchpad.net/ubuntu/+archive/primary/+files/google-glog_0.3.4-0.1.dsc

Prepare and Upload Package
~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Unzip ``google-glog_0.3.4-0.1.debian.tar.xz``
2. Unzip ``google-glog_0.3.4.orig.tar.gz``
3. Structure:

   -  tmp

      -  glog-0.3.4

         -  debian

      -  google-glog\_0.3.4.orig.tar.gz

4. ``cd tmp/glog-0.3.4``
5. run ``sudo debuild -S -kE35F12BA`` (public launchpad gpg key id, gpg
   –list-keys)
6. ``cd ..``
7. ``dput ppa:john-doe/google-glog google-glog_0.3.4-0trusty_source.changes``

Debuild Options
^^^^^^^^^^^^^^^

Build source package (required for Launchpad)

::

    sudo debuild -S

Do not sign package

::

    -us    Do not sign the source package.
    -uc    Do not sign the .changes file.
    -S     Source-only package
    -b     Binary-only package
    -sa    Forces the inclusion of the original source.

-  `dpkg-buildpackage Manpage`_
-  `dpkg-genchanges Manpage`_

Sign package after

::

    debsign -k E35F12BA *.dsc
    debsign -k E35F12BA *.changes

Sign debian package

::

    debsigs --sign=origin -k E35F12BA libgoogle-glog-dev_0.3.4-0trusty_amd64.deb

`debsigs Manpage`_

Debian Package
~~~~~~~~~~~~~~

::

    pdebuild --architecture amd64 --buildresult ~/pbuilder_ws/ws/glog/tmp/ --pbuilderroot "sudo DIST=trusty ARCH=amd64"

.. _dpkg-buildpackage Manpage: http://manpages.ubuntu.com/manpages/xenial/en/man1/dpkg-buildpackage.1.html
.. _dpkg-genchanges Manpage: http://manpages.ubuntu.com/manpages/zesty/man1/dpkg-genchanges.1.html
.. _debsigs Manpage: http://manpages.ubuntu.com/manpages/xenial/man1/debsigs.1p.html
