Tricks
======

.. toctree::
   :maxdepth: 4

   docs/git/git
   docs/programming/programming
   docs/python/python
   docs/rtd/rtd
   docs/ros/ros
   docs/ubuntu/ubuntu
   docs/debian_packaging/debian_packaging


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`